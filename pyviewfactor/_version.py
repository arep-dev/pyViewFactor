"""Version info for pyViewFactor"""
version_info = 0, 0, 16

# Nice string for the version
__version__ = '.'.join(map(str, version_info))
