# v0.0.16

new contributor: Samuel Sourisseau !

## Breaking Changes
* Updated dependancies [breaking]:
    * `pyvista == 0.42` `(> 0.38)`

## Fixed
* `get_visibility` function: Some particular configurations were missing conditions to check vivibility. 


# v0.0.15

## Added 
* `get_visibility_MT` function: Ray triangle intersection with the Möller Trumbore algorthm
* `get_visibility_MT` function: added to the docs

## Fixed 
* `get_visibility` funciotn: added a strict argument, to check all the points, not aonly the centroids

# v0.0.14

## Fixed
* `compute_viewfactor` function: a fixed decimal truncature was yielding wrong results, removed. 
* `compute_viewfactor` function: in the case of cells sharing an edge, fixed the "virtual displacement direction. It was done according to the adjacent cell normal direction, it's now according to the cell centroids direction. 

## Added 
* Some coments to the code, new and translations (from french to english),
* Precision in the documentation.

# v0.0.12 (27.1.2023)

## Fixed
* Dependency issue with `pandas`. Will now work with `pandas >= 1.5.0`

## Added
* new precision parameter for the `compute_viewfactor` function : `epsilon` to get the desired precision. 
* New example in the documentation : Thermal Comfort MRT with Pyvista's Doorman.

# v0.0.11 (19.8.2022)

## Added 
* use of `numba` library in the `integrand` and `commpute_viewfactor` functions. 
* Documentation accessible at https://arep-dev.gitlab.io/pyViewFactor/ 

# v0.0.10 (8.8.2022)

## Added
* `__init__.py` and `_version.py`

# v0.0.8 (8.8.2022)

## Fixed (1 change)
* `README.md` modifications for images absolute URL (for Pypi)

# v0.0.7 (8.8.2022)

## Fixed (5 changes)
* Remove debugging prints throughout the code
* Update examples
* Typo in authors names
* Update obstruction function (typo)
* Update `get_visibility_raytrace` function call (typo)
* Update `README.md`
